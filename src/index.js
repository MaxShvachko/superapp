import React from "react";
import ReactDOM from "react-dom";
import {Layout} from "./modules/Layout.jsx";
import "./style/index.less"


ReactDOM.render(<Layout />, document.getElementById("root"));