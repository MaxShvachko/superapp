import React, { Component } from "react";
import PropTypes from 'prop-types';
import ReactDOM from "react-dom";


class Layout extends Component {

   state = {
       enter: true
   } ;

    changeState = () => {
        this.setState(prevState =>{
            return {enter: !prevState.enter}
        });
        console.log(this.state.enter)
    };

    render() {
     return (
         <div className={"wrapper"}>
             <header className={"header"}>{"Hello"}</header>
             <div className={"flex-wrapper"}>
                 <div className={"flex-wrapper__item"}></div>
                 <div className={"flex-wrapper__item"}>
                     <button onClick={this.changeState} className={"btn"}>{`${this.state.enter}`}</button>
                 </div>
             </div>
             <footer className={"footer"}></footer>
         </div>


     )
    }
}

export {Layout}