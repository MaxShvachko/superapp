"use strict";
require("babel-polyfill");
var currency;
async function getDataFromApi() {
    const response = await fetch(`https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5`);
    currency = await response.json();
};

let getCurrency = document.getElementById('getCurrency');
getCurrency.addEventListener('click', countTheCurrency);

getDataFromApi();
let inputFrom = document.getElementById('inputCurrency');
let inputTo = document.getElementById('outputResult');
let selectFrom = document.getElementById('fromCurrency');
let selectTo = document.getElementById('toCurrency');
let result = null;
inputFrom.oninput=()=>{validator(inputFrom)};
function validator(input) {
    let mess = document.getElementById("messageMoney");
    let temp = input.value;
    let length = temp.length;
    let num = isFinite(temp);
    if (!num) {
        if(selectElementLanguage.value==="ru"){mess.innerHTML = "Введите число"}else
        {mess.innerHTML="Input number!!!"}
        input.value = "";
        return false;
    } else if (num < 0) {
        if(selectElementLanguage.value==="ru"){mess.innerHTML = "Введено отрицательное число"}else
        {mess.innerHTML="Need positive number"}
        input.value = "";
        return false;
    } else if (length > 13) {
        if(selectElementLanguage.value==="ru"){mess.innerHTML = "Слишком большое число"}else
        {mess.innerHTML="Need short number"}
        input.value = temp.slice(0, -1);
        return false;
    } else {
        mess.innerHTML = "";
        return true;
    }
}
function countTheCurrency() {
        if (selectFrom.value !== selectTo.value) {
            if (selectFrom.value === 'UAH') {
                for (let i = 0; i < currency.length; i++) {
                    if (currency[i].ccy === selectTo.value) {
                        result = inputFrom.value / currency[i].sale;
                        inputTo.value = result.toFixed(4);
                    }
                }
            } else if (selectTo.value === 'UAH') {
                for (let i = 0; i < currency.length; i++) {
                    if (currency[i].ccy === selectFrom.value) {
                        result = inputFrom.value * currency[i].sale;
                        inputTo.value = result.toFixed(4);
                    }
                }
            } else {
                for (let i = 0; i < currency.length; i++) {
                    if (currency[i].ccy === selectFrom.value) {
                        result = inputFrom.value * currency[i].buy;
                    }
                }
                for (let i = 0; i < currency.length; i++) {
                    if (currency[i].ccy === selectTo.value) {
                        result = result / currency[i].sale;
                        inputTo.value = result.toFixed(4);
                    }
                }
            }
        } else if (selectFrom.value === selectTo.value) {
            inputTo.value = inputFrom.value;
        }

}


