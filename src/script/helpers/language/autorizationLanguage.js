export default {
    en:{ text:{
            textAuthorization:"Authorization",
            labelLogin:"Login:",
            labelPassword:"Password:",
            registrationBtnAvt:"Registration",
            logInBtn:"Log In",
            forgottenPassword:"Forgotten Password",
            textForgottenPassword:"Password recovery",
            forgottenLabelLogin:"Login:",
            forgottenLabelPassword:"Keyword:",
            forgottenPassLabel:"Your password:",
            getPassword:"Get Password",
        },
        placeholder:{
            loginAvt:"write your login",
            passwordAvt:"write your password",
            forgottenLoginAvt:"write your login ",
            kayWordAvt:"write your keyword",
            forgottenPasswordMessage:"Your password"
        }

    },
    ru:{ text: {
            textAuthorization:"Авторизация",
            labelLogin:"Логин:",
            labelPassword:"Пароль:",
            registrationBtnAvt:"Регистрация",
            logInBtn:"Войти",
            forgottenPassword:"Забыл пароль",
            textForgottenPassword:"Восстановление пароля",
            forgottenLabelLogin:"Логин:",
            forgottenLabelPassword:"Секретное слово:",
            forgottenPassLabel:"Ваш пароль:",
            getPassword:"Получить пароль",
        },
        placeholder:{
            loginAvt:"Введите ваш логин",
            passwordAvt:"Введите ваш пароль",
            forgottenLoginAvt:"Введите ваш логин",
            kayWordAvt:"Введите секретное слово",
            forgottenPasswordMessage:"Тут будет ваш пароль",
        }
    }
};
