module.exports = {
  port: process.env.PORT || 3000,
  routing: {
    authorization: '/authorization',
    registration: '/registration',
    groupStudent: '/groupStudent',
    getAllGroups: '/getAllGroups',
    accountSetting: '/accountSetting',
    groups: '/groups',
    table: '/',
    update: '/update',
    delete: '/delete',
    accountUpdate: '/account-update',
    forgottenPassword: '/forgotten-password',
    deleteGroup: '/delete-group',
    studentClear: '/student-clear',
    updateGroup: '/update-group',
    sendImage: '/send-image',
    resetSettings: '/reset-settings'

  }
};
