function authorization (request, response, client) {
  var user = {
    login: request.body.login,
    password: request.body.password,
  };
  var baseLogin;
  var basePassword;
  client.query(`SELECT * FROM teachers WHERE login = '${user.login}';`, [], function (err, result) {
    for (var key in result.rows) {
      baseLogin = result.rows[key].login;
      basePassword = result.rows[key].password;
      teacherId = result.rows[key].teachers_id;
    }
    if (baseLogin === `${user.login}` && basePassword === `${user.password}`) {
      response.json(result.rows)
    } else {
      authorized = "";
      response.status(401).send('Unauthorized ');
    }
  });
}

function registration(request, response, client) {
  var user = {
    login: request.body.login,
    password: request.body.password,
    email: request.body.email,
    phone: request.body.phone,
    keyword: request.body.keyword
  };
  client.query(`SELECT * FROM teachers WHERE login = '${user.login}';`, [], function (err, result) {

    var baselogin;
    for (var key in result.rows) {

      baselogin = result.rows[key].login;
    }
    if (baselogin !== `${user.login}`) {
      var newUser = `INSERT INTO teachers(login, password, email, phone_number,keyword) VALUES ('${user.login}', '${user.password}', '${user.email}', '${user.phone}','${user.keyword}')`;
      client.query(newUser, []);
      response.status(200).send('Ok');
    } else {
      response.status(400).send('Bad Request ');
    }
  });
}

function getTeacherInfo(request, response, client) {
  client.query(`SELECT * FROM teachers WHERE teachers_id = '${teacherId}';`, [], function (err, result) {
    if(result.rows !== undefined) {
      response.json(result.rows);
    }
  });
}

function updateTeacherData(request, response, client) { // TODO check data in client
  var teacherId = {
    id: request.body.teachers_id
  };
  var teachersSqlColumn = [
    "login",
    "password",
    "email",
    "phone_number",
    "about_myself",
  ];
  var queryComand = "";
  var valueCounter = 0;
  var counterLink = 1;

  var user = {
    login: request.body.login,
    password: request.body.password,
    email: request.body.email,
    phone_number: request.body.phone,
    about_myself: request.body.aboutMyself,
  };

  var upgradeSQL = [];

  Object.keys(user).forEach(function (key) {
    if (!(this[key].length === 0)) {
      upgradeSQL.push(`${this[key]}`);
      // if ()
      queryComand += teachersSqlColumn[valueCounter] + "= $" + counterLink + ",";

      counterLink++
    }
    valueCounter++;
  }, user);

  queryComand = queryComand.substring(0, queryComand.length - 1);
  console.log(teacherId.id, upgradeSQL, queryComand);

  client.query(`UPDATE teachers SET ${queryComand} WHERE teachers_id = ${teacherId.id}`,
    upgradeSQL,
    function (err, result) {
      if (err) {
        console.log(err); // TODO fix console log
      }
      console.log(result);
    });
}

function insertIntoGroups(request, response, client) {
  var newGroup = `INSERT INTO groups(groupname, teacher_id) VALUES 
    ('${request.body.groupName}', ${request.body.teachers_id})`;
  client.query(newGroup, [],function (err, result) {
    if (err) {
      console.log(err);
    }
  });
  client.query(`SELECT * FROM groups WHERE groupname = '${request.body.groupName}';`, [], function (err, result) {

    response.json(result.rows[0].groups_id);
  });
}

function checkForgottenPassword(request, response, client) {
  var user = {
    login: request.body.login,
    keyword: request.body.keyword,
  };
  client.query(`SELECT * FROM teachers WHERE login = '${user.login}';`, [], function (err, result) {
    var baseLogin;
    var basekeyword;
    var  basePassword ;
    for (var key in result.rows) {
      baseLogin = result.rows[key].login;
      basekeyword = result.rows[key].keyword;
      basePassword = result.rows[key].password;
    }
    console.log(basePassword, basekeyword, baseLogin);
    if (baseLogin === `${user.login}` && basekeyword === `${user.keyword}`) {
      authorized = request.body.login;
      response.json(basePassword);
    } else {
      authorized = "";
      response.status(401).send('Unauthorized ');
    }
  });
}

function updateTeacherIcon(request, response, client) {
  console.log(teacherId);
  client.query(`UPDATE teachers SET teacher_icon = '${request.body.img}' WHERE teachers_id = ${teacherId};`, [], (err, res) => {
    if (err) {
      response.status(502).send("SERVER ERROR");
    } else {
      response.json(request.body.img);
    }
  });
}

  module.exports = {
  authorization,
  registration,
  getTeacherInfo,
  insertIntoGroups,
  updateTeacherData,
  checkForgottenPassword,
  updateTeacherIcon,


};